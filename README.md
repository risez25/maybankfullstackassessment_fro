# Live Preview:
## https://ng-boilerplate.mdbootstrap.com/

# Technologies used

* Angular 8
* Bootstrap 4
* MDBootstrap Angular
* NgRx

# Installation:

**Install:**
npm install

**Run:**
npm start